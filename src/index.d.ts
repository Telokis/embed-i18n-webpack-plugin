declare type ObjectOf<T> = { [key: string]: T };

declare type LangDefinition = {
    [key: string]: string | LangDefinition;
};

interface EmbedI18nPluginOptions {
    /**
     * Specifies the translate function to use.
     * Default is `"__"`
     */
    funcName?: string;

    /**
     * Fallback to use if a key is missing inside _langs_
     */
    fallbackLangDefinition?: LangDefinition;

    /**
     * Required to aggregate a key for all langs.
     */
    allLangDefinitions?: ObjectOf<LangDefinition>;
}

declare class EmbedWebpackI18nPlugin {
    constructor(langDefinition: LangDefinition, options?: EmbedI18nPluginOptions);
}

interface TranslationOptions {
    returnArrayWithAllLangs?: boolean;
    allowResultToNotBeString?: boolean;
}

type TranslationFunctionBase = (key: string, options?: TranslationOptions) => string;
type TranslationFunctionObj = (
    key: string,
    options: TranslationOptions & {
        returnArrayWithAllLangs?: false;
        allowResultToNotBeString: true;
    },
) => ObjectOf<string>;
type TranslationFunctionArr = (
    key: string,
    options: TranslationOptions & {
        returnArrayWithAllLangs: true;
        allowResultToNotBeString?: false;
    },
) => Array<[string, string]>;
type TranslationFunctionObjArr = (
    key: string,
    options: TranslationOptions & {
        returnArrayWithAllLangs: true;
        allowResultToNotBeString: true;
    },
) => Array<[string, ObjectOf<string>]>;

type TranslationFunction = TranslationFunctionObj &
    TranslationFunctionArr &
    TranslationFunctionObjArr &
    TranslationFunctionBase;

export { TranslationFunction };

export = EmbedWebpackI18nPlugin;
