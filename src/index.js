const ConstDependency = require("webpack/lib/dependencies/ConstDependency");
const {
    toConstantDependency,
} = require("webpack/lib/javascript/JavascriptParserHelpers");
const { isObject, deepAssign } = require("./helpers");

const PLUGIN_NAME = "EmbedWebpackI18nPlugin";

function makeObjectFromBasicExpression(expr) {
    if (expr.expression.type !== "ObjectExpression") {
        return null;
    }

    const res = {};

    for (let i = 0; i < expr.expression.properties.length; i++) {
        const property = expr.expression.properties[i];

        res[property.key.name] = property.value.value;
    }

    return res;
}

function get(obj, path, fallback = null) {
    const pathMembers = path.split(".");

    let node = obj;
    for (let i = 0; i < pathMembers.length; i++) {
        const elem = pathMembers[i];

        if (elem in node) {
            node = node[elem];
        } else {
            return fallback;
        }
    }

    return node;
}

module.exports = class EmbedWebpackI18nPlugin {
    constructor(lang, options) {
        this.lang = lang;
        this.name = options?.funcName ?? "__";
        this.fallbackLangDefinition = options?.fallbackLangDefinition ?? null;
        this.allLangDefinitions = options?.allLangDefinitions ?? null;
    }

    getValue(key, warnings, errors, location, lang) {
        const { fallbackLangDefinition: fallbackLang } = this;
        let value = get(lang, key);
        const fallbackValue = fallbackLang && get(fallbackLang, key);

        if (value === null && fallbackValue !== null) {
            warnings.push(
                `[${PLUGIN_NAME}]: The key "${key}" doesn't exist in the supplied lang definition. Using fallback.\n` +
                    `    at ${location}`,
            );
            value = fallbackValue;
        }

        if (value === null) {
            warnings.push(
                `[${PLUGIN_NAME}]: The key "${key}" doesn't exist in either the provided nor fallback definitions. Using source text.\n` +
                    `    at ${location}`,
            );
            value = key;
        }

        if (isObject(value) && fallbackValue) {
            if (isObject(fallbackValue)) {
                value = deepAssign({}, fallbackValue, value);
            } else {
                errors.push(
                    `[${PLUGIN_NAME}]: The key "${key}" is an 'object' in your definition but isn't one in the fallback definition. Make sure your definitions have the same schemas.\n` +
                        `    at ${location}`,
                );
                return null;
            }
        }

        return value;
    }

    isGoodValue(key, value, options, warnings, errors, location) {
        if (value === null) {
            return false;
        }

        if (
            typeof value !== "string" &&
            options.allowResultToNotBeString !== true
        ) {
            errors.push(
                `[${PLUGIN_NAME}]: Expected value for key "${key}" to be a 'string' but got '${typeof value}'. If this is the expected behavior, use option 'allowResultToNotBeString'.\n` +
                    `    at ${location}`,
            );

            return false;
        }

        if (
            typeof value === "string" &&
            options.allowResultToNotBeString === true
        ) {
            warnings.push(
                `[${PLUGIN_NAME}]: Expected value for key "${key}" is a 'string' but the option 'allowResultToNotBeString' was specified. You don't need it.\n` +
                    `    at ${location}`,
            );
        }

        return true;
    }

    apply(compiler) {
        const { lang, name, allLangDefinitions: allLangs } = this;

        compiler.hooks.compilation.tap(
            PLUGIN_NAME,
            (compilation, { normalModuleFactory }) => {
                compilation.dependencyTemplates.set(
                    ConstDependency,
                    new ConstDependency.Template(),
                );

                const handler = (parser) => {
                    parser.hooks.call.for(name).tap(PLUGIN_NAME, (expr) => {
                        const location = `${parser.state.current.resource} (line ${expr.loc.start.line}, column ${expr.loc.start.column})`;

                        if (
                            expr.arguments.length < 1 ||
                            expr.arguments.length > 2
                        ) {
                            compilation.errors.push(
                                `[${PLUGIN_NAME}]: The i18n function '${name}' expects 1 or 2 arguments but got ${expr.arguments.length}.\n` +
                                    `    at ${location}`,
                            );
                            return false;
                        }

                        const keyExpr = parser.evaluateExpression(
                            expr.arguments[0],
                        );

                        if (!keyExpr.isString()) {
                            compilation.errors.push(
                                `[${PLUGIN_NAME}]: The first argument of the translate function must be a string.\n` +
                                    `    at ${location}`,
                            );
                            return false;
                        }

                        const key = keyExpr.asString();

                        let options = {};

                        if (expr.arguments.length === 2) {
                            const optsExpr = parser.evaluateExpression(
                                expr.arguments[1],
                            );
                            const opts =
                                makeObjectFromBasicExpression(optsExpr);

                            if (opts === null) {
                                compilation.errors.push(
                                    `[${PLUGIN_NAME}]: The second argument of the translate function must be a static object.\n` +
                                        `    at ${location}`,
                                );
                                return false;
                            }

                            options = opts;
                        }

                        let value = null;

                        if (options.returnArrayWithAllLangs === true) {
                            if (!allLangs) {
                                compilation.errors.push(
                                    `[${PLUGIN_NAME}]: Option returnArrayWithAllLangs was true but 'allLangs' was not specified in the plugin constructor.\n` +
                                        `    at ${location}`,
                                );
                                return false;
                            }

                            const entries = Object.entries(allLangs);
                            value = [];

                            for (let i = 0; i < entries.length; i++) {
                                const [langName, curLang] = entries[i];
                                const val = this.getValue(
                                    key,
                                    [],
                                    compilation.errors,
                                    location,
                                    curLang,
                                );

                                if (
                                    !this.isGoodValue(
                                        key,
                                        val,
                                        options,
                                        compilation.warnings,
                                        compilation.errors,
                                        location,
                                    )
                                ) {
                                    return false;
                                }

                                value.push([langName, val]);
                            }
                        } else {
                            value = this.getValue(
                                key,
                                compilation.warnings,
                                compilation.errors,
                                location,
                                lang,
                            );

                            if (
                                !this.isGoodValue(
                                    key,
                                    value,
                                    options,
                                    compilation.warnings,
                                    compilation.errors,
                                    location,
                                )
                            ) {
                                return false;
                            }
                        }

                        return toConstantDependency(
                            parser,
                            `(${JSON.stringify(value)})`,
                        )(expr);
                    });
                };

                normalModuleFactory.hooks.parser
                    .for("javascript/auto")
                    .tap(PLUGIN_NAME, handler);
                normalModuleFactory.hooks.parser
                    .for("javascript/dynamic")
                    .tap(PLUGIN_NAME, handler);
                normalModuleFactory.hooks.parser
                    .for("javascript/esm")
                    .tap(PLUGIN_NAME, handler);
            },
        );
    }
};
