function isObject(value) {
    return typeof value === "object" && value !== null && !Array.isArray(value);
}
exports.isObject = isObject;

exports.deepAssign = function deepAssign(target, ...sources) {
    const result = Object.assign(target, ...sources);
    const keys = Object.keys(result);

    for (let i = 0; i < keys.length; ++i) {
        const key = keys[i];
        const sourcesToMerge = sources
            .filter((source) => key in source && isObject(source[key]))
            .map((source) => source[key]);

        if (sourcesToMerge.length) {
            result[key] = deepAssign({}, result[key], ...sourcesToMerge);
        }
    }

    return result;
};
