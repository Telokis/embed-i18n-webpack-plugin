const path = require("path");
const fs = require("fs");
const HtmlWebpackPlugin = require("html-webpack-plugin");

const env = process.env.NODE_ENV;

const LANGS_DIR = path.resolve(__dirname, "langs");

const files = fs.readdirSync(LANGS_DIR);

const langs = files.reduce((obj, file) => {
    const content = require(path.resolve(LANGS_DIR, file));
    const lang = path.basename(file, ".json");

    obj[lang] = content;

    return obj;
}, {});

const I18n = require("../../src");

const availableLangs = Object.keys(langs);

const mainConfig = {
    mode: env,

    output: {
        path: path.resolve(__dirname, "build"),
        filename: "[name].bundle.js",
    },

    devServer: {
        contentBase: path.resolve(__dirname, "public"),
        compress: false,
        port: 3500,
        hot: true,
        open: true,
        clientLogLevel: "silent",
    },

    plugins: [
        new HtmlWebpackPlugin({
            template: path.resolve(__dirname, "public", "index.html"),
            inject: false,
            minify: true,
        }),
        new I18n(langs.en, {
            allLangDefinitions: langs,
            funcName: "t",
        }),
    ],

    resolve: {
        extensions: [".js", ".jsx"],
    },
};

const translationConfigs = availableLangs.map((lang) => ({
    entry: {
        main: path.resolve(__dirname, "src", "index.js"),
    },

    mode: env,

    output: {
        path: path.resolve(__dirname, "build", lang),
        filename: `${lang}.[name].bundle.js`,
        publicPath: "",
    },

    module: {
        rules: [],
    },

    plugins: [
        new HtmlWebpackPlugin({
            template: path.resolve(__dirname, "public", "lang_index.html"),
            inject: true,
            minify: false,
        }),
        new I18n(langs[lang], {
            fallbackLangDefinition: langs.en,
            allLangDefinitions: langs,
            funcName: "t",
        }),
    ],

    resolve: {
        extensions: [".js", ".jsx"],
    },
}));

module.exports = [mainConfig, ...translationConfigs];
