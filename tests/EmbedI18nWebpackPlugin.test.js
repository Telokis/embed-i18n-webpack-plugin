const path = require("path");
const I18n = require("../src");
const { execute } = require("./helpers/webpackWrapper");

describe("EmbedI18nWebpackPlugin", () => {
    describe("simple usage", () => {
        it("should not do anything if there is nothing to translate", async () => {
            const stats = await execute({
                entry: path.resolve(__dirname, "inputFiles", "simple.js"),
                plugins: [new I18n({})],
            });

            expect(stats.warnings.length).toBe(0);
            expect(stats.errors.length).toBe(0);

            expect(stats.requiredAssets).toMatchInlineSnapshot(`
                            Object {
                              "main.js": Object {
                                "test": "Working",
                              },
                            }
                    `);
        });

        it("should perform a basic translation", async () => {
            const stats = await execute({
                entry: path.resolve(
                    __dirname,
                    "inputFiles",
                    "singleTranslate.js",
                ),
                plugins: [
                    new I18n({
                        word: "mot",
                    }),
                ],
            });

            expect(stats.warnings.length).toBe(0);
            expect(stats.errors.length).toBe(0);

            expect(stats.requiredAssets).toMatchInlineSnapshot(`
                            Object {
                              "main.js": Object {
                                "word": "mot",
                              },
                            }
                    `);
        });

        it("should use the fallback definition if the key isn't found", async () => {
            const stats = await execute({
                entry: path.resolve(
                    __dirname,
                    "inputFiles",
                    "singleTranslate.js",
                ),
                plugins: [
                    new I18n(
                        {},
                        {
                            fallbackLangDefinition: {
                                word: "Simple word",
                            },
                        },
                    ),
                ],
            });

            expect(stats.warnings.length).toBe(1);
            expect(stats.warnings).toMatchSnapshot();

            expect(stats.errors.length).toBe(0);

            expect(stats.requiredAssets).toMatchInlineSnapshot(`
                            Object {
                              "main.js": Object {
                                "word": "Simple word",
                              },
                            }
                    `);
        });

        it("should fallback to the key if the key isn't found and no fallback is provided", async () => {
            const stats = await execute({
                entry: path.resolve(
                    __dirname,
                    "inputFiles",
                    "singleTranslate.js",
                ),
                plugins: [new I18n({}, {})],
            });

            expect(stats.warnings.length).toBe(1);
            expect(stats.warnings).toMatchSnapshot();

            expect(stats.errors.length).toBe(0);

            expect(stats.requiredAssets).toMatchInlineSnapshot(`
                            Object {
                              "main.js": Object {
                                "word": "word",
                              },
                            }
                    `);
        });

        it("should fallback to the key if the key isn't found and is not in the fallback definition", async () => {
            const stats = await execute({
                entry: path.resolve(
                    __dirname,
                    "inputFiles",
                    "singleTranslate.js",
                ),
                plugins: [
                    new I18n(
                        {},
                        {
                            fallbackLangDefinition: {
                                wording: "Simple word",
                            },
                        },
                    ),
                ],
            });

            expect(stats.warnings.length).toBe(1);
            expect(stats.warnings).toMatchSnapshot();

            expect(stats.errors.length).toBe(0);

            expect(stats.requiredAssets).toMatchInlineSnapshot(`
                            Object {
                              "main.js": Object {
                                "word": "word",
                              },
                            }
                    `);
        });

        it("should fallback to the key if the key isn't found and no options are provided", async () => {
            const stats = await execute({
                entry: path.resolve(
                    __dirname,
                    "inputFiles",
                    "singleTranslate.js",
                ),
                plugins: [new I18n({})],
            });

            expect(stats.warnings.length).toBe(1);
            expect(stats.warnings).toMatchSnapshot();

            expect(stats.errors.length).toBe(0);

            expect(stats.requiredAssets).toMatchInlineSnapshot(`
                            Object {
                              "main.js": Object {
                                "word": "word",
                              },
                            }
                    `);
        });

        it("should support a custom name for the translation function", async () => {
            const stats = await execute({
                entry: path.resolve(
                    __dirname,
                    "inputFiles",
                    "customFuncName.js",
                ),
                plugins: [
                    new I18n({ word: "mot" }, { funcName: "__translate" }),
                ],
            });

            expect(stats.warnings.length).toBe(0);
            expect(stats.errors.length).toBe(0);

            expect(stats.requiredAssets).toMatchInlineSnapshot(`
                            Object {
                              "main.js": Object {
                                "word": "mot",
                              },
                            }
                    `);
        });
    });

    describe("option returnArrayWithAllLangs", () => {
        it("should allow the translation function to be replaced by an array of translations", async () => {
            const definitions = {
                fr: {
                    sentence: "phrase",
                },
                en: {
                    sentence: "sentence",
                },
            };
            const stats = await execute({
                entry: path.resolve(
                    __dirname,
                    "inputFiles",
                    "returnArrayWithAllLangs.js",
                ),
                plugins: [
                    new I18n(definitions.fr, {
                        allLangDefinitions: definitions,
                        fallbackLangDefinition: definitions.en,
                    }),
                ],
            });

            expect(stats.warnings.length).toBe(0);
            expect(stats.errors.length).toBe(0);

            expect(stats.requiredAssets).toMatchInlineSnapshot(`
                Object {
                  "main.js": Object {
                    "sentence": Array [
                      Array [
                        "fr",
                        "phrase",
                      ],
                      Array [
                        "en",
                        "sentence",
                      ],
                    ],
                  },
                }
            `);
        });

        it("should allow the translation function to be replaced by an array of translations definition if allowResultToNotBeString is used", async () => {
            const definitions = {
                fr: {
                    sentence: { word: "mot", character: "caractère" },
                },
                en: {
                    sentence: { word: "word", character: "character" },
                },
            };
            const stats = await execute({
                entry: path.resolve(
                    __dirname,
                    "inputFiles",
                    "arrayOfAllLangsWithNonString.js",
                ),
                plugins: [
                    new I18n(definitions.fr, {
                        allLangDefinitions: definitions,
                        fallbackLangDefinition: definitions.en,
                    }),
                ],
            });

            expect(stats.warnings.length).toBe(0);
            expect(stats.errors.length).toBe(0);

            expect(stats.requiredAssets).toMatchInlineSnapshot(`
                Object {
                  "main.js": Object {
                    "sentence": Array [
                      Array [
                        "fr",
                        Object {
                          "character": "caractère",
                          "word": "mot",
                        },
                      ],
                      Array [
                        "en",
                        Object {
                          "character": "character",
                          "word": "word",
                        },
                      ],
                    ],
                  },
                }
            `);
        });

        it("should allow the translation function to be replaced by an array of translations definition if allowResultToNotBeString is used and the fallback should be used to provide missing keys", async () => {
            const definitions = {
                fr: {
                    sentence: { word: "mot", character: "caractère" },
                },
                en: {
                    sentence: {
                        word: "word",
                        character: "character",
                        number: "number",
                    },
                },
            };
            const stats = await execute({
                entry: path.resolve(
                    __dirname,
                    "inputFiles",
                    "arrayOfAllLangsWithNonString.js",
                ),
                plugins: [
                    new I18n(definitions.fr, {
                        allLangDefinitions: definitions,
                        fallbackLangDefinition: definitions.en,
                    }),
                ],
            });

            expect(stats.warnings.length).toBe(0);
            expect(stats.errors.length).toBe(0);

            expect(stats.requiredAssets).toMatchInlineSnapshot(`
                Object {
                  "main.js": Object {
                    "sentence": Array [
                      Array [
                        "fr",
                        Object {
                          "character": "caractère",
                          "number": "number",
                          "word": "mot",
                        },
                      ],
                      Array [
                        "en",
                        Object {
                          "character": "character",
                          "number": "number",
                          "word": "word",
                        },
                      ],
                    ],
                  },
                }
            `);
        });
    });

    describe("option allowResultToNotBeString", () => {
        it("should allow the translation function to be replaced by a json object", async () => {
            const stats = await execute({
                entry: path.resolve(
                    __dirname,
                    "inputFiles",
                    "allowResultToNotBeString.js",
                ),
                plugins: [
                    new I18n({
                        sentence: { word: "mot", character: "caractère" },
                    }),
                ],
            });

            expect(stats.warnings.length).toBe(0);
            expect(stats.errors.length).toBe(0);

            expect(stats.requiredAssets).toMatchInlineSnapshot(`
                    Object {
                      "main.js": Object {
                        "sentence": Object {
                          "character": "caractère",
                          "word": "mot",
                        },
                      },
                    }
                `);
        });

        it("should merge the fallback value if it also is an object", async () => {
            const stats = await execute({
                entry: path.resolve(
                    __dirname,
                    "inputFiles",
                    "allowResultToNotBeString.js",
                ),
                plugins: [
                    new I18n(
                        {
                            sentence: { word: "mot", character: "caractère" },
                        },
                        {
                            fallbackLangDefinition: {
                                sentence: {
                                    word: "word",
                                    character: "char",
                                    number: "number",
                                },
                            },
                        },
                    ),
                ],
            });

            expect(stats.warnings.length).toBe(0);
            expect(stats.errors.length).toBe(0);

            expect(stats.requiredAssets).toMatchInlineSnapshot(`
                Object {
                  "main.js": Object {
                    "sentence": Object {
                      "character": "caractère",
                      "number": "number",
                      "word": "mot",
                    },
                  },
                }
            `);
        });

        it("should emit a warning if the option is specified while the value is a string", async () => {
            const stats = await execute({
                entry: path.resolve(
                    __dirname,
                    "inputFiles",
                    "allowResultToNotBeString.js",
                ),
                plugins: [new I18n({ sentence: "phrase" })],
            });

            expect(stats.warnings.length).toBe(1);
            expect(stats.warnings).toMatchSnapshot();

            expect(stats.errors.length).toBe(0);

            expect(stats.requiredAssets).toMatchInlineSnapshot(`
                Object {
                  "main.js": Object {
                    "sentence": "phrase",
                  },
                }
            `);
        });
    });

    describe("error reporting", () => {
        it("should report an error if option returnArrayWithAllLangs is used but allLangDefinitions is not specified", async () => {
            const stats = await execute({
                entry: path.resolve(
                    __dirname,
                    "inputFiles",
                    "returnArrayWithAllLangs.js",
                ),
                plugins: [new I18n({})],
            });

            expect(stats.warnings.length).toBe(0);

            expect(stats.errors.length).toBe(1);
            expect(stats.errors).toMatchSnapshot();

            expect(stats.requiredAssets).toStrictEqual({});
        });

        it("should report an error if option returnArrayWithAllLangs is used but a value is an object and allowResultToNotBeString is not set", async () => {
            const definitions = {
                fr: {
                    sentence: "phrase",
                },
                en: {
                    sentence: {
                        word: "word",
                    },
                },
            };
            const stats = await execute({
                entry: path.resolve(
                    __dirname,
                    "inputFiles",
                    "returnArrayWithAllLangs.js",
                ),
                plugins: [
                    new I18n(definitions.fr, {
                        allLangDefinitions: definitions,
                        fallbackLangDefinition: definitions.en,
                    }),
                ],
            });

            expect(stats.warnings.length).toBe(0);

            expect(stats.errors.length).toBe(1);
            expect(stats.errors).toMatchSnapshot();

            expect(stats.requiredAssets).toStrictEqual({});
        });

        it("should report an error if the key is not a static string", async () => {
            const stats = await execute({
                entry: path.resolve(
                    __dirname,
                    "inputFiles",
                    "variableTranslate.js",
                ),
                plugins: [new I18n({ word: "mot" })],
            });

            expect(stats.warnings.length).toBe(0);

            expect(stats.errors.length).toBe(1);
            expect(stats.errors).toMatchSnapshot();

            expect(stats.requiredAssets).toStrictEqual({});
        });

        it("should report an error if the fallback value is not an object", async () => {
            const stats = await execute({
                entry: path.resolve(
                    __dirname,
                    "inputFiles",
                    "allowResultToNotBeString.js",
                ),
                plugins: [
                    new I18n(
                        {
                            sentence: { word: "mot", character: "caractère" },
                        },
                        {
                            fallbackLangDefinition: {
                                sentence: "sentence",
                            },
                        },
                    ),
                ],
            });

            expect(stats.warnings.length).toBe(0);

            expect(stats.errors.length).toBe(1);
            expect(stats.errors).toMatchSnapshot();

            expect(stats.requiredAssets).toStrictEqual({});
        });

        it("should report an error if the options passed to the translation function is not a static object", async () => {
            const stats = await execute({
                entry: path.resolve(
                    __dirname,
                    "inputFiles",
                    "invalidOptions.js",
                ),
                plugins: [new I18n({ one: "un" })],
            });

            expect(stats.warnings.length).toBe(0);

            expect(stats.errors.length).toBe(1);
            expect(stats.errors).toMatchSnapshot();

            expect(stats.requiredAssets).toStrictEqual({});
        });

        it("should report an error if the value is not a string", async () => {
            const stats = await execute({
                entry: path.resolve(
                    __dirname,
                    "inputFiles",
                    "singleTranslate.js",
                ),
                plugins: [new I18n({ word: { val: "mot" } })],
            });

            expect(stats.warnings.length).toBe(0);

            expect(stats.errors.length).toBe(1);
            expect(stats.errors).toMatchSnapshot();

            expect(stats.requiredAssets).toStrictEqual({});
        });

        it("should report an error if less that 1 parameter is provided to the translation function", async () => {
            const stats = await execute({
                entry: path.resolve(__dirname, "inputFiles", "noParameter.js"),
                plugins: [new I18n({ word: "mot" })],
            });

            expect(stats.warnings.length).toBe(0);

            expect(stats.errors.length).toBe(1);
            expect(stats.errors).toMatchSnapshot();

            expect(stats.requiredAssets).toStrictEqual({});
        });

        it("should report an error if more that 2 parameters are provided to the translation function", async () => {
            const stats = await execute({
                entry: path.resolve(
                    __dirname,
                    "inputFiles",
                    "threeParameters.js",
                ),
                plugins: [new I18n({ word: "mot" })],
            });

            expect(stats.warnings.length).toBe(0);

            expect(stats.errors.length).toBe(1);
            expect(stats.errors).toMatchSnapshot();

            expect(stats.requiredAssets).toStrictEqual({});
        });
    });
});
