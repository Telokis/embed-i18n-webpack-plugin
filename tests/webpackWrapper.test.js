const path = require("path");
const {
    makeDirectoryName,
    updateConfig,
    execute,
} = require("./helpers/webpackWrapper");

describe("makeDirectoryName", () => {
    it("should return a string", () => {
        expect(typeof makeDirectoryName()).toBe("string");
    });
});

describe("updateConfig", () => {
    describe("object config", () => {
        it("should handle a config without output", () => {
            const config = {};
            updateConfig(config);

            expect(typeof config.output.path).toBe("string");
        });

        it("should handle a config with output but no path", () => {
            const config = {
                output: {},
            };
            updateConfig(config);

            expect(typeof config.output.path).toBe("string");
        });

        it("should handle a config with output and path", () => {
            const config = {
                output: {
                    path: "",
                },
            };
            updateConfig(config);

            expect(typeof config.output.path).toBe("string");
        });

        it("should use output.path as a path for the build directory", () => {
            const config = {
                output: {
                    path: "simple/suffix",
                },
            };
            updateConfig(config);

            expect(
                config.output.path.endsWith(path.normalize("/simple/suffix")),
            ).toBe(true);
        });

        it("should update config.mode if not specified", () => {
            const config = {
                output: {
                    path: "simple/suffix",
                },
            };
            updateConfig(config);

            expect(config.mode).toBe("development");
        });

        it("should override output.libraryTarget", () => {
            const config = {
                output: {
                    path: "simple/suffix",
                    libraryTarget: "umd",
                },
            };
            updateConfig(config);

            expect(config.output.libraryTarget).toBe("commonjs2");
        });

        it("should not change the mode if specified", () => {
            const config = {
                mode: "production",
                output: {
                    path: "simple/suffix",
                },
            };
            updateConfig(config);

            expect(config.mode).toBe("production");
        });
    });

    describe("array config", () => {
        it("should handle an array of configs without output", () => {
            const config = [{}, {}];
            updateConfig(config);

            expect(typeof config[0].output.path).toBe("string");
            expect(typeof config[1].output.path).toBe("string");
        });

        it("should handle an array of configs with output but no path", () => {
            const config = [
                {
                    output: {},
                },
                {
                    output: {},
                },
            ];
            updateConfig(config);

            expect(typeof config[0].output.path).toBe("string");
            expect(typeof config[1].output.path).toBe("string");
        });

        it("should handle an array of configs with output and path", () => {
            const config = [
                {
                    output: { path: "" },
                },
                {
                    output: { path: "" },
                },
            ];
            updateConfig(config);

            expect(typeof config[0].output.path).toBe("string");
            expect(typeof config[1].output.path).toBe("string");
        });

        it("should handle an array of configs with mixed outputs", () => {
            const config = [
                {
                    output: { path: "" },
                },
                {
                    output: {},
                },
                {},
            ];
            updateConfig(config);

            expect(typeof config[0].output.path).toBe("string");
            expect(typeof config[1].output.path).toBe("string");
            expect(typeof config[2].output.path).toBe("string");
        });

        it("should use output.path as a path for the build directory even in an array", () => {
            const config = [
                {
                    output: {
                        path: "simple/suffix",
                    },
                },
                {
                    output: {
                        path: "single",
                    },
                },
                {
                    output: {},
                },
            ];
            updateConfig(config);

            expect(
                config[0].output.path.endsWith(
                    path.normalize("/simple/suffix"),
                ),
            ).toBe(true);
            expect(
                config[1].output.path.endsWith(path.normalize("/single")),
            ).toBe(true);
            expect(typeof config[2].output.path).toBe("string");
        });
    });
});

describe("execute", () => {
    it("should compile a very basic file", async () => {
        const stats = await execute({
            entry: path.resolve(__dirname, "inputFiles", "simple.js"),
        });

        expect(stats.warnings.length).toBe(0);
        expect(stats.errors.length).toBe(0);

        expect(stats.requiredAssets).toMatchSnapshot();
    });
});
