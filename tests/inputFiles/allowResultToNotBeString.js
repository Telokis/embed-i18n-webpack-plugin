/* global __:false */

module.exports = {
    sentence: __("sentence", { allowResultToNotBeString: true }),
};
