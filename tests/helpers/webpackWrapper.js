const webpack = require("util").promisify(require("webpack"));
const path = require("path");
const fs = require("fs");
const rimraf = require("rimraf");

const TEST_BUILD_DIRECTORY = path.resolve(__dirname, "..", "builds");

function makeDirectoryName() {
    return `tmp-${Math.floor(Math.random() * 1000000)}`;
}
exports.makeDirectoryName = makeDirectoryName;

function omitLocation(arr, match) {
    return arr.map((val) =>
        val
            .split("\n")
            .filter((line) => !line.includes(match))
            .join("\n"),
    );
}
exports.omitLocation = omitLocation;

function requireDirectory(dir) {
    const res = {};

    if (fs.existsSync(dir) && fs.statSync(dir).isDirectory()) {
        const files = fs.readdirSync(dir);

        for (let i = 0; i < files.length; ++i) {
            const f = files[i];
            const p = path.resolve(dir, f);

            if (fs.statSync(p).isDirectory()) {
                res[f] = requireDirectory(p);
            } else if (f.endsWith(".js")) {
                // eslint-disable-next-line global-require
                res[f] = require(p);
            } else {
                res[f] = fs.readFileSync(p);
            }
        }
    }

    return res;
}
exports.requireDirectory = requireDirectory;

function updateConfig(config) {
    const tmpName = makeDirectoryName();
    const buildDir = path.resolve(TEST_BUILD_DIRECTORY, tmpName);

    if (!Array.isArray(config)) {
        config = [config];
    }

    for (let i = 0; i < config.length; i++) {
        const conf = config[i];

        if (!("output" in conf)) {
            conf.output = {};
        }

        if (!("optimization" in conf)) {
            conf.optimization = {};
        }

        if (!("mode" in conf)) {
            conf.mode = "development";
        }

        if ("path" in conf.output) {
            conf.output.path = path.resolve(buildDir, conf.output.path);
        } else {
            conf.output.path = buildDir;
        }

        if (!("devtool" in conf)) {
            conf.devtool = false;
        }

        if (!("emitOnErrors" in conf.optimization)) {
            conf.optimization.emitOnErrors = false;
        }

        conf.output.libraryTarget = "commonjs2";
    }

    return buildDir;
}
exports.updateConfig = updateConfig;

exports.execute = async function execute(config) {
    const buildDir = updateConfig(config);
    const res = {};

    try {
        const stats = await webpack(config);
        const strToOmit = path.normalize(
            "embed-i18n-webpack-plugin/tests/inputFiles",
        );

        res.assets = stats.compilation.assets;
        res.warnings = omitLocation(stats.compilation.warnings, strToOmit);
        res.errors = omitLocation(stats.compilation.errors, strToOmit);

        res.requiredAssets = requireDirectory(buildDir);
    } catch (e) {
        await rimraf.sync(buildDir);
        throw e;
    }

    await rimraf.sync(buildDir);

    return res;
};
