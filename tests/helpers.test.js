const { isObject, deepAssign } = require("../src/helpers");

describe("isObject", () => {
    it("should return true for an object", () => {
        expect(isObject({})).toStrictEqual(true);
    });

    it("should return false for null", () => {
        expect(isObject(null)).toStrictEqual(false);
    });

    it("should return false for an array", () => {
        expect(isObject([])).toStrictEqual(false);
    });

    it("should return false for a number", () => {
        expect(isObject(18)).toStrictEqual(false);
    });
});

describe("deepAssign", () => {
    it("should work for shallow objects", () => {
        const obj1 = { test: "hello" };
        const obj2 = { val: 18 };
        const merged = deepAssign(obj1, obj2);

        expect(merged).toStrictEqual({
            test: "hello",
            val: 18,
        });
    });

    it("should only alter the first argument", () => {
        const obj1 = { test: "hello" };
        const obj2 = { val: 18 };
        const merged = deepAssign({}, obj1, obj2);

        expect(merged).toStrictEqual({
            test: "hello",
            val: 18,
        });
        expect(obj1).toStrictEqual({ test: "hello" });
        expect(obj2).toStrictEqual({ val: 18 });
    });

    it("should treat the last argument as having the highest precedence", () => {
        const obj1 = { test: "hello" };
        const obj2 = { test: 18 };
        const merged = deepAssign({}, obj1, obj2);

        expect(merged).toStrictEqual({
            test: 18,
        });
    });

    it("should work with multiple parameters", () => {
        const obj1 = { test: "hello" };
        const obj2 = { test: 18 };
        const obj3 = { val: 42 };
        const merged = deepAssign({}, obj1, obj2, obj3);

        expect(merged).toStrictEqual({
            test: 18,
            val: 42,
        });
    });

    it("should properly merge arbitrary depth", () => {
        const obj1 = { test: "hello", val1: { deep1: "123" } };
        const obj2 = { val: 18, val1: { deep2: "456" } };
        const merged = deepAssign({}, obj1, obj2);

        expect(merged).toStrictEqual({
            test: "hello",
            val: 18,
            val1: {
                deep1: "123",
                deep2: "456",
            },
        });
    });

    it("should treat the last argument as having the highest precedence even deeply", () => {
        const obj1 = { test: "hello", val1: { deep1: "123" } };
        const obj2 = { val: 18, val1: { deep1: "456" } };
        const merged = deepAssign({}, obj1, obj2);

        expect(merged).toStrictEqual({
            test: "hello",
            val: 18,
            val1: {
                deep1: "456",
            },
        });
    });

    it("should deeply alter the first parameter", () => {
        const obj1 = { test: "hello", val1: { deep1: "123" } };
        const obj2 = { val: 18, val1: { deep1: "456", deep2: "789" } };
        const merged = deepAssign(obj1, obj2);

        expect(merged).toStrictEqual({
            test: "hello",
            val: 18,
            val1: {
                deep1: "456",
                deep2: "789",
            },
        });
        expect(merged).toStrictEqual(obj1);
        expect(obj2).toStrictEqual({
            val: 18,
            val1: { deep1: "456", deep2: "789" },
        });
    });
});
