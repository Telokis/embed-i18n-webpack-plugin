[![npm][npm]][npm-url]
[![pipeline][pipeline]][pipeline-url]
[![coverage][coverage]][coverage-url]

# EmbedI18nWebpackPlugin

_This plugin is inspired by [I18nWebpackPlugin](https://webpack.js.org/plugins/i18n-webpack-plugin/) which is deprecated._

This [Webpack](https://webpack.js.org/) plugin will embed translated data directly into your bundle.  
No more need to dynamically translate strings that never change!

## Usage

Start by installing the plugin by doing

```sh
npm i -D embed-i18n-webpack-plugin
```

In your `webpack.config.js`, you can use the plugin like so:

```js
const EmbedI18nWebpackPlugin = require("embed-i18n-webpack-plugin");

// Then, in your config object:
plugins: [
    // ...
    new EmbedI18nWebpackPlugin(langDefinition, options),
];
```

where

-   `langDefinition` is a json object containing the translations to use.
-   `options` is optional and may contain the following properties:
    -   `options.funcName` is the name of your translation function (default: `__`)
    -   `options.fallbackLangDefinition` is a definition to use if a key is missing from `langDefinition`. (If not specified, the key will be used instead)
    -   `options.allLangDefinitions` is an object where keys are lang names and values are the corresponding definitions. This is used in conjunction with `returnArrayWithAllLangs`, see [Advanced usage](#advanced-usage).

Once the plugin is configured, you can use a translation function inside your code and it will automatically be replaced at build time.

Here is a small concrete example:

```js
// webpack.config.js

// I start by defining my translations
const frenchDef = {
    greeting: {
        hello: "Salut, monde.",
    },
};
const englishDef = {
    greeting: {
        hello: "Hello, world.",
    },
};

// Then, inside the config object, I instantiate the plugin
plugins: [
    new EmbedI18nWebpackPlugin(frenchDef, {
        funcName: "__t",
        fallbackLangDefinition: englishDef,
    }),
];

// index.js
console.log(__t("greeting.hello"));

// build/index.js
console.log("Salut, monde.");
```

## Advanced usage

Since you might want to do more complicated operations with your translation function, it accepts a second, optional object parameter specifying the way this specific translation should occur:

```js
__t(key, options);
```

-   `key` MUST be a static string. Anything else is not supported yet.
-   `options` is optional and may contain the following properties altering the translation process:

    -   `options.returnArrayWithAllLangs` will make the function return an array containing all translations of `key` in the form `[languageName, value]`.
        This option **requires** that the option `allLangDefinitions` is specified when instantiating the plugin. It will throw otherwise.
        This option will always make the translation function return an array containing all langs, even if it has to fallback to the default definition or the key.
    -   `options.allowResultToNotBeString` will allow the translation function to return a subset of the language definition instead of a string. By default, the build will throw if something other than a string is about to be inserted within the code.

The options `returnArrayWithAllLangs` and `allowResultToNotBeString` can be combined in order to return an array containing non-strings.

Here is another example showcasing those functionalities:

```js
// webpack.config.js

// I start by defining my translations
const definitions = {
    fr: {
        greeting: {
            hello: "Salut, monde.",
        },
    },
    en: {
        greeting: {
            hello: "Hello, world.",
        },
        onlyEnglish: "This is fine.",
    },
};

// Then, inside the config object, I instantiate the plugin
plugins: [
    new EmbedI18nWebpackPlugin(definitions.fr, {
        funcName: "__t",
        fallbackLangDefinition: definitions.en,
        allLangDefinitions: definitions,
    }),
];

// index.js

// Becomes "Salut, monde."
console.log(__t("greeting.hello"));

// Becomes "This is fine." (Fallback to english)
console.log(__t("onlyEnglish"));

// Becomes "greeting.unknown" (Fallback to the key)
console.log(__t("greeting.unknown"));

// Throws a build error because the value is not a string
console.log(__t("greeting"));

// Becomes { hello: "Salut, monde." }
console.log(__t("greeting", { allowResultToNotBeString: true }));

// Becomes "Salut, monde." but prints a warning
console.log(__t("greeting.hello", { allowResultToNotBeString: true }));

// Becomes [["en", "Hello, world."], ["fr", "Salut, monde."]]
console.log(__t("greeting.hello", { returnArrayWithAllLangs: true }));

// The option returnArrayWithAllLangs always return an array containing all langs,
// even if it has to fallback to the default definition or the key
// Becomes [["en", "This is fine."], ["fr", "This is fine."]]
console.log(__t("onlyEnglish", { returnArrayWithAllLangs: true }));

// Becomes [
//     ["en", { hello: "Hello, world." }],
//     ["fr", { hello: "Salut, monde." }]
// ]
console.log(__t("greeting", { returnArrayWithAllLangs: true, allowResultToNotBeString: true }));
```

[npm]: https://img.shields.io/npm/v/embed-i18n-webpack-plugin.svg
[npm-url]: https://npmjs.com/package/embed-i18n-webpack-plugin
[pipeline]: https://gitlab.com/Telokis/embed-i18n-webpack-plugin/badges/master/pipeline.svg
[pipeline-url]: https://gitlab.com/Telokis/embed-i18n-webpack-plugin/-/commits/master
[coverage]: https://gitlab.com/Telokis/embed-i18n-webpack-plugin/badges/master/coverage.svg
[coverage-url]: https://gitlab.com/Telokis/embed-i18n-webpack-plugin/-/commits/master
