module.exports = {
    extends: ["@telokys/eslint-config"],
    rules: {
        "no-console": "off",
        "no-new": "off",
        "new-cap": "off",
        "jest/prefer-strict-equal": "off",
    },
    parserOptions: {
        sourceType: "module",
        ecmaVersion: 2020,
    },
};
